## Nomad SDK

This package includes the `NomadContext`, a management system for Nomad core
contracts, which inherits from the `MultiProvider`. `NomadContext` allows
developers to easily interact with the Nomad system on any number of networks.

## Building

```
yarn build
```
