# Changelog

### Unreleased

### 2.0.0-rc.7

- chore: bump configuration to v0.1.0-rc.10

### 2.0.0-rc.6

- chore: bump multi-provider to v1.0.0-rc.4

### 2.0.0-rc.5

- refactor: simpler connection logic (deleting `reconnect`)
- refactor: improved generics in types using contexts
- bug: getReplica only accepts domain number
- chore: bump configuration to v0.1.0-rc.9

### 2.0.0-rc.4

- feature: Added `CoreContracts.deployHeight` getter
