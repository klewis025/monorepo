# Changelog

### Unreleased

- fix: add goerli overrides to overrides.json
- feature: Added core and bridge checks
- chore: bump configuration to v0.1.0-rc.10
- fix: core check for watchers
- chore: bump multi-provider to v1.0.0-rc.4
- chore: bump sdk to v2.0.0-rc.6
- chore: bump sdk-govern to v1.0.0-rc.5
