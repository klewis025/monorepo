# Changelog

### Unreleased

### 1.0.0-rc.6

- chore: bump configuration to v0.1.0-rc.10

### 1.0.0-rc.5

- chore: bump sdk to v2.0.0-rc.6

### 1.0.0-rc.4

- chore: bump configuration to v0.1.0-rc.9