## Nomad Govern SDK

This package includes the `CallBatch`, a management system for Nomad governance
actions. `CallBatch` allows developers to easily instruct the Nomad governance
system to interact with contracts on any network. It is intended to be used in
conjunction with a `NomadContext` object.

## Building

```
yarn build
```
