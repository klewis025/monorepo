## Nomad Bridge SDK

This package includes the `BridgeContext`, a management system for Nomad core
contracts, which inherits from the `MultiProvider`. `BridgeContext` allows
developers to easily interact with the Nomad Token Bridge on any number of
networks.

## Building

```
yarn build
```
