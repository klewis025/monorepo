# Changelog

### Unreleased

### 1.0.0-rc.7

- chore: bump configuration to v0.1.0-rc.10

### 1.0.0-rc.6

- chore: bump sdk to v2.0.0-rc.6

### 1.0.0-rc.5

- refactor: simpler connection logic (deleting `reconnect`)
- refactor: improved generics in types using contexts
- chore: bump configuration to v0.1.0-rc.9

### 1.0.0-rc.4

- feature: Added `BridgeContracts.deployHeight` getter
